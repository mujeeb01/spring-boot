package com.practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = "com.practice")
public class SchoolRegisteryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolRegisteryApplication.class, args);
	}

}
