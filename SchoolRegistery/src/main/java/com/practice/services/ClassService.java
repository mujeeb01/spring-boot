package com.practice.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.Repository.ClassRepository;

@Service
public class ClassService {

    @Autowired
    private ClassRepository classRepository;

    public List<Class> getAllClasses() {
        return classRepository.findAll();
    }

    public Class getClassById(Long id) {
        return classRepository.findById(id).orElse(null);
    }

    public void saveClass(Class class1) {
        classRepository.save(class1);
    }

    public void deleteClassById(Long id) {
        classRepository.deleteById(id);
    }

    public List<Class> getClassesByTeacherId(Long teacherId) {
        return classRepository.findByTeacherId(teacherId);
    }

}