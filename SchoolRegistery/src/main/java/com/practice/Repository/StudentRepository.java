package com.practice.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practice.entities.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

}